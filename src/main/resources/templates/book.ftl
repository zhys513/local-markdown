<!DOCTYPE html>
<html lang="UTF-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content=""> 
<title>markdown编辑器</title>
<link href="${ctx}/static/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="${ctx}/static/css/page/blog.css" rel="stylesheet">
<link href="${ctx}/static/css/markdown/simplemde.min.css" rel="stylesheet">
<link href="${ctx}/static/css/font-awesome.min.css" rel="stylesheet">
 
</head>

<body>
	<div class="container">
		<div class="row" style="padding-top: 10px">
			<form class="form-horizontal" id="form_save">
 
				<div class="form-group"> 
					<div class="col-sm-10">
						<textarea id="textarea_content" name="content" class="form-control">${content!''}</textarea>
						<input type="hidden" name="contentPath" value="${contentPath!''}" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="button" onclick="save()" class="btn btn-default">保存</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->
 	<script src="${ctx}/static/js/jquery/jquery.min.js"></script>
	<script src="${ctx}/static/js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/bootstrap/plugins/bootbox.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/common/common.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/common/myValidation.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/markdown/simplemde.min.js"></script>
	<script type="text/javascript">
			var simplemde = new SimpleMDE({
				element: $("#textarea_content")[0],
				autoDownloadFontAwesome:false,
				toggleSideBySide : true,
				spellChecker: false,
				//showIcons: ['code', 'table','horizontal-rule'], 
				//hideIcons:['fullscreen'] ,
				toolbar: [{
					name: "bold",
					action: SimpleMDE.toggleBold,
					className: "fa fa-bold",
					title: "加粗",
				},
				{
					name: "italic",
					action: SimpleMDE.toggleItalic,
					className: "fa fa-italic",
					title: "斜体",
				},
				{
					name: "heading-smaller",
					action: SimpleMDE.toggleHeadingSmaller,
					className: "fa fa-header",
					title: "缩小标题",
				},
				{
					name: "heading-bigger",
					action: SimpleMDE.toggleHeadingBigger,
					className: "fa fa-lg fa-header",
					title: "增大标题",
				},
				"|",
				{
					name: "code",
					action: SimpleMDE.toggleCodeBlock,
					className: "fa fa-code",
					title: "代码块",
				},
				{
					name: "quote",
					action: SimpleMDE.toggleBlockquote,
					className: "fa fa-quote-left",
					title: "引用",
				},
				{
					name: "unordered-list",
					action: SimpleMDE.toggleUnorderedList,
					className: "fa fa-list-ul",
					title: "无序列表",
				},
				{
					name: "ordered-list",
					action: SimpleMDE.toggleOrderedList,
					className: "fa fa-list-ol",
					title: "有序列表",
				},
				"|",
				{
					name: "link",
					action: SimpleMDE.drawLink,
					className: "fa fa-link",
					title: "插入链接",
				},
				{
					name: "image",
					action: SimpleMDE.drawImage,
					className: "fa fa-picture-o",
					title: "插入图片",
				},
				{
					name: "table",
					action: SimpleMDE.drawTable,
					className: "fa fa-table",
					title: "插入表格",
				},
				{
					name: "horizontal-rule",
					action: SimpleMDE.drawHorizontalRule,
					className: "fa fa-minus",
					title: "插入水平线",
				},
				"|",
				{
					name: "preview",
					action: SimpleMDE.togglePreview,
					className: "fa fa-eye no-disable",
					title: "全屏预览",
				},
				{
					name: "side-by-side",
					action: SimpleMDE.toggleSideBySide,
					className: "fa fa-columns no-disable no-mobile",
					title: "实时预览",
				},
				"|",
				{
					name: "save",
					action: function customFunction(editor){ 
						save();
					},
					className: "fa fa-save",
					title: "保存",
				},
				{
					name: "first",
					action: function customFunction(editor){
						window.location.href="/";
					},
					className: "fa fa-home",
					title: "首页",
				},
				"|"
			]
			 
			});
			simplemde.toggleSideBySide();
			simplemde.toggleFullScreen();
			
			
			function save(){ 
				var validResult = myValidation.commonValid("form_save", {
					showInfo : function(ele, mes) {
						bootbox.alert(mes);
					}
				});
				if (!validResult) {
					return;
				}
				var content = simplemde.value();
				if(content.trim().length == 0){
					bootbox.alert("请输入内容");
				}
				$("#textarea_content").val(content);
				$.post("/add",$("#form_save").serialize(), function(result){
					if(result.code == 200){
						bootbox.alert("保存成功", function(){
							//location.href= "/";
						});
					}else{
						bootbox.alert(result.mes);
					}
				})
			}
		
		</script>
</body>

</html>