package cn.zhys513.mk;
 
import javax.servlet.ServletContext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@SpringBootApplication
@EnableConfigurationProperties
public class Application extends WebMvcConfigurationSupport {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
	}
	
	@Override
	public void setServletContext(@Nullable ServletContext servletContext) {
        String path = servletContext.getContextPath();
        if("/".equals(path.trim())){
            path="";
        } 
        servletContext.setAttribute("ctx", path);
		super.setServletContext(servletContext);
	}
}
